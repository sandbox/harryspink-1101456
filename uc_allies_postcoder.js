/*
 * Allies postcoder integration
 *
 * Author: Harry Spink (harry {dot} spink {at} commcam {dot} co {dot} uk)
 */

$(document).ready(function(){
   /*
    * Capture the clicking of the postcode lookup button
    */
    $('input#edit-panes-billing-billing-postal-code-search').click(function(){
        if($(this).val()==''){
            alert('You must enter a postcode');
            return;
        }
        $.ajax({
            type: 'GET',
            url:  '/cart/ajax/postcoder',
            data: 'postcode=' + $('#edit-panes-billing-billing-postal-code').val(),
            dataType: 'json',
            success: function(eventResponse){
                //Check to see if we have an error
                if(eventResponse.status=='error'){
                    //Display the error message to the user
                    alert(eventResponse.reason);
                } else {
                    //Create a select box with the address list
                    var addressList = eventResponse.addresses[0];
                    var propertySelector = '<option value="none">Select building name / number</option>';

                    //Create the select box options based on the response
                    for(property in addressList.premise){
                        propertySelector += '<option value="'+addressList.premise[property].premise+'">'+addressList.premise[property].premise + ', ' + addressList.street +'</option>';
                    }

                    //Prepare the HTML to send to the browser
                    propertySelector = '<select id="edit-panes-billing-billing-premise-select">' + propertySelector + '</select>';

                    //Remove the previous results, if exists
                    $('.currentPostCodeLookupResults').remove();
                    //Send it to the browser
                    $('input#edit-panes-billing-billing-postal-code-search').parent().parent().after('<tr class="currentPostCodeLookupResults"><td class="field-label">Select Property:</td><td>'+propertySelector+'</td></tr>');

                    //Onclick populate the input boxes
                    $('#edit-panes-billing-billing-premise-select').change(function(){
                        $('input#edit-panes-billing-billing-street1').val($(this).val() + ', ' + addressList.street);

                        if($('input#edit-panes-billing-billing-street2').val() == ''){
                            $('input#edit-panes-billing-billing-street2').val(addressList.dependent_locality);
                        }

                        if($('input#edit-panes-billing-billing-city').val() == ''){
                            $('input#edit-panes-billing-billing-city').val(addressList.post_town);
                        }

                        if($('#edit-panes-billing-billing-zone').val() == ''){
                            $('#edit-panes-billing-billing-zone').val(addressList.county);
                        }
                    });
                }
            }
        });
    });
});