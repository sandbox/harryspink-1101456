<?
/*
 * Allies postcoder integration - Administration interface
 *
 * Author: Harry Spink (harry {dot} spink {at} commcam {dot} co {dot} uk)
 */


function uc_allies_postcoder_admin(){
    $form = array();

    /*
     * General settings
     */
    $form['general'] = array(
        '#type' => 'fieldset',
        '#description' => t('Postcoder settings')
    );
    $form['general']['uc_allies_postcoder_username'] = array(
        '#type' => 'textfield',
        '#title' => t('Username'),
        '#default_value' => variable_get('uc_allies_postcoder_username', ''),
        '#description' => t('Postcoder username')
    );
    $form['general']['uc_allies_postcoder_password'] = array(
        '#type' => 'textfield',
        '#title' => t('Password'),
        '#default_value' => variable_get('uc_allies_postcoder_password', ''),
        '#description' => t('Postcoder password')
    );
    $form['general']['uc_allies_postcoder_identify'] = array(
        '#type' => 'textfield',
        '#title' => t('Identifier'),
        '#default_value' => variable_get('uc_allies_postcoder_identify', ''),
        '#description' => t('Unique identifier for this location')
    );
    $form['general']['uc_allies_postcoder_soapWsdl'] = array(
        '#type' => 'textfield',
        '#title' => t('WSDL'),
        '#default_value' => variable_get('uc_allies_postcoder_soapWsdl', ''),
        '#description' => t('Postcoder WSDL Address')
    );

    return system_settings_form($form);
}