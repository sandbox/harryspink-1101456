<?php
/*
 * Allies postcoder integration
 *
 * Author: Harry Spink (harry {dot} spink {at} commcam {dot} co {dot} uk)
 */

/*
 * Implementation of hook_menu
 */
function uc_allies_postcoder_menu(){
    $items = array();

    //Admin interface page
    $items['admin/settings/postcoder'] = array(
        'title' => t('Postcoder Settings'),
        'page callback' => 'drupal_get_form',
        'page arguments' => array('uc_allies_postcoder_admin'),
        'file' => 'uc_allies_postcoder_admin.inc',
        'access arguments' => array('access content'),
        'type' => MENU_NORMAL_ITEM,
    );

    //Admin interface page
    $items['cart/ajax/postcoder'] = array(
        'title' => t('Postcoder results'),
        'page callback' => 'uc_allies_postcoder_lookup_postcode',
        'access arguments' => array('access content'),
        'type' => MENU_NORMAL_ITEM,
    );

    return $items;
}

/*
 * Implementation of hook_form_alter to modify the postcode field
 * Simple way to capture the path to add a Javascript file if required
 */
function uc_allies_postcoder_form_alter(&$form, &$form_state, $form_id){
    if($form_id == 'uc_cart_checkout_form'){
        //Include the javascrupt required for the postcode lookup
        drupal_add_js(drupal_get_path('module', 'uc_allies_postcoder').'/uc_allies_postcoder.js');

        //Bring the postcode up after the username and password
        $form['panes']['billing']['billing_first_name']['#weight'] = -4;
        $form['panes']['billing']['billing_last_name']['#weight'] = -3;
        $form['panes']['billing']['billing_postal_code']['#weight'] = -2;

        //Add a new submit button with an ID we can use to search for a postalcode
        $form['panes']['billing']['billing_postal_code_search'] = array(
            '#type' => 'button',
            '#value' => t('Lookup postcode'),
            '#weight' => -1,
            '#attributes' => array('id' => 'postcoder_lookup', 'onclick' => 'return false;')
        );
    }
}

function uc_allies_postcoder_lookup_postcode(){
    //Check we have a postcode argument in the URL
    if(!isset($_GET['postcode']))
        return drupal_json(array('status' => 'error', 'reason' => 'No postcode specified'));

    //Prepare the user details to pass over to the service
    $postCoder_username = variable_get('uc_allies_postcoder_username', '');
    $postCoder_password = variable_get('uc_allies_postcoder_password', '');
    $postCoder_soapWsdl = variable_get('uc_allies_postcoder_soapWsdl', '');
    $postCoder_identify = variable_get('uc_allies_postcoder_identify', '');
    $postCoder_postcode = trim($_GET['postcode']);

    //Check all the variables are populated
    if($postCoder_username==''||$postCoder_password==''||$postCoder_soapWsdl=='')
        return drupal_json(array('status' => 'error', 'reason' => 'Invalid server configuration'));

    //Include the nu-soap library as packaged with Postcoder
    require_once('nusoap/lib/nusoap.php');

    //Create the client instance and then the proxy instance
    $client = new soap_client($postCoder_soapWsdl,'wsdl');
    $proxy = $client->getproxy();
    
    //Check for errors  - if so, let the user know now.
    if ($proxy->getError()) {
        watchdog('postcoder', $proxy->getError);
        return drupal_json(array('status' => 'error', 'reason' => 'Postcoder error'));
    }

    //All seems good - lets try and do a postcode lookup
    $result = $proxy->getMatchAddress($postCoder_postcode, $postCoder_identify, $postCoder_username, $postCoder_password);

    //Check to see if we have any soap errors
    if($proxy->fault){
        watchdog('postcoder', $proxy->fault);
        return drupal_json(array('status' => 'error', 'reason' => 'Invalid server configuration'));
    } else {
        //Check to see if we have any errors from postcoder
        if($proxy->getError()){
            watchdog('postcoder', $proxy->getError());
            return drupal_json(array('status' => 'error', 'reason' => 'Invalid server configuration'));
        } else {
            //All is good - we can now send back the JSON object
            return drupal_json($result);
        }
    }

}